FROM svdvoort/prognosais_glioma:1.0.2

COPY ./run_prognosais.sh /run_prognosais.sh
RUN chmod +x /run_prognosais.sh

ENTRYPOINT [ "/bin/bash" ]
