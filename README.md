# prognosais-fastr

Docker adapted from the original [PrognosAIs_glioma](https://github.com/Svdvoort/PrognosAIs_glioma) to allow use in fastr networks. This tool 
replaces the original interface to work with command line arguments.

Usage:

`` 
docker registry.gitlab.com/radiology/infrastructure/docker-images/fastr-prognosais /run_prognosais.sh {flair.nii.gz} {t2.nii.gz} {t1.nii.gz} {t1gd.nii.gz} {output_directory}
`` 

Make sure all file paths are mounted as expected. Use the tool description `prognosais.xml` for use in [Fastr](https://fastr.readthedocs.io/).